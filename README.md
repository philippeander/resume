# Philippe Anderson

EMAIL: philippecontactme@gmail.com

SITE: Stonmanblog.wordpress.com 


__________________________________________________________________________________________

* Development of 3D / 2D activities using programming in Unity Game Engine.
* C# gameplay programming.
* Developing Applications in Android Studio. 

## EXPERIENCE
* Instituto Alfa e Beto, Uberlândia— Junior Game Programmer

	AUGUST 2017 - PRESENT

	Development of educational games and applications


## EDUCATION
* UNIFACISA, Campina Grande-PB — TECHNOLOGIST IN DIGITAL GAMES

	AUGUST 2013 - JULY 2017

## SKILS
* Programming in Unity 3D
* Android Studio
* Programming Construct 2
* C#
* Java
* Hardware Maintenance
* 3D Modeling and Sculpture
* Illustration
* Texturization of Assets
* 2D Animation (modular animation)
* 3D Animation
* Game Design
* Vector Illustration

#### Experience with:

* Java Script
* MySQL
* Php
* html 5 / css3
* Python


## PROJECTS


### JAMS
#### GAME BOY JAM, August 2014 - 
	Game Castle of Souls / Programming and Art.

#### LUDUM DARE 30 JAM, August 2014 - 
	Game Khivy / Programming and Art.

#### GLOBAL GAME JAM, January 2015 - 
	Game America Land / Programming and Art.

#### LUDUM DARE 32 JAM, April 2015 - 
	Think like a pigeon / Programming and Art.

#### KOLKS GAME JAM, April 2015 - 
	Game Desafio da Água / Programming and Art.

#### FETEC GAME JAM, October 2015 - 
	Game Save The Water / Programming and Art.
	(Note: "Save the water" won the "Best Game" (2nd Place) and "Best Design" (1st 
	Place)).

#### GLOBAL GAME JAM, January 2016 - 
	Game Mage’s Magic / Programming and Art.

#### NORDEVS GAME JAM,July 2016 - 
	Canaria / Programming and Art.


